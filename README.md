Par défaut JavaScript :
- que dans un navigateur
- que du frontend

Node :
- interprête le JavaScript en dehors du navigateur (dans le terminal)
- logiciel qui permet d'exécuter du JavaScript
- permet de lancer des serveurs (côté backend)

NPM : Node Package Manager
- installe des modules (bibliothèque, package)

Framework :
- regroupement de bibliothèques qui imposent une méthode de travail

Express :
- micro-framework en JavaScript pour faire du backend
- accéder à une base de données
- créer un API
- faire un serveur d'authentification