import validator from 'validator'; // ES6

const phoneNumberList = [
  '0761574355',
  '+33765887645',
  '+3376587645',
  '0461574355',
];

for (const eachPhone of phoneNumberList) {
  const isValid = validator.isMobilePhone(eachPhone, 'fr-FR');
  //console.log(eachPhone, isValid)
}

// utiliser filter pour ne garder que les numéros valides
const listFiltered = phoneNumberList.filter(
  (eachPhone) => {
    // retourne un booléen
    const isValid = validator.isMobilePhone(eachPhone, 'fr-FR');
    return isValid;
  }
)
console.log(listFiltered)